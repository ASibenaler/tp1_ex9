## Exercice 9

# Objectif
L'objectif de cet exercice est de déposer le package créé précédement sur Pypi, pour pouvoir par la suite faciliter son installation.

# Réalisation
On part du projet créé à l'exercice 7, auquel on a ajouté le fichier *setup.py* créé à l'exercice 8. On modifie le nom du package dans ce fichier par un nom qui ne correspond à aucun repository Pypi.
On génère tout d'abord les archives que l'on va déposer sur Pypi :

	python3 -m build

On dépose ensuite les archives générées avec l'outil *twine* que l'on a préalablement installé :

	python3 -m pip install twine
	python3 -m twine upload --repository testpypi dist/*
	
Une fois que le projet a bien été déposé sur Pypi, son lien est affiché :

	https://test.pypi.org/project/EX9ComplexCalculatorArnaudSibenaler/
	
# Installation
Il est à présent possible d'installer le package créé depuis Pypi.
Pour cela, on créé une environnement virtuel avec venv :

	python3 -m venv venv
	source ./venv/bin/activate

On installe le package :

	 pip install -i https://test.pypi.org/simple/ EX9ComplexCalculatorArnaudSibenaler
